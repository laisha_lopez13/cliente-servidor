// SERVIDOR.XOCHITL LAISHA LOPEZ LOPEZ

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include<ctype.h>

#define MYPORT 3490    
#define MAXBUFLEN 200  
#define BACKLOG 10   


#define MAX 100

int longitud;
char salida[MAX];
char actual[MAX];
char actualposterior[MAX];
char revisada[MAX]; 

void num_letra(char revisada[]);


int sockfd; 
int newfd; 
char buf[MAXBUFLEN];
struct sockaddr_in my_addr; 
struct sockaddr_in their_addr;
socklen_t sin_size;

char *token = NULL;
char vector [MAX];
int cont,loop,a;

void crear_socket();
void atributos();
void socket_name();
void escuchar();
void aceptar();
void recibir();
void enviar(char cadena[]);

void crear_socket()
{
 

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("socket");
    exit(1);
  }
}

void atributos() 
{
  my_addr.sin_family = AF_INET; 

  my_addr.sin_port = htons(MYPORT);

  my_addr.sin_addr.s_addr = INADDR_ANY; 
  
  bzero(&(my_addr.sin_zero), 8);  
}

void socket_name()
{
  if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1)
  {
    perror("bind");
    exit(1);
  }
}

void escuchar()
{
  if (listen(sockfd, BACKLOG) == -1)
  {
    perror("listen");
    exit(1);
  }  
}

void aceptar()
{
  sin_size = sizeof(struct sockaddr_in);
    
   
  if ((newfd = accept(sockfd, (struct sockaddr *)&their_addr,&sin_size)) == -1)
  {
    perror("accept");
    exit(1); 
  }
  printf("server: conexion desde: %s\n", inet_ntoa(their_addr.sin_addr));  
}

void recibir()
{
  if ((recv(newfd, buf, MAXBUFLEN, 0)) == -1)
  {
    perror("recv");
    exit(1);
  }
}

void enviar(char cadena[])
{
  if (send(newfd, cadena, MAXBUFLEN, 0) == -1)
  perror("send");
  exit(1);
}

int main()
{ 
  crear_socket();
  
  atributos();
  
  socket_name();

  escuchar();
 
  while(1) 
  {
    cont=0;
    
    aceptar();
    if (!fork())
    { 
    
      
      recibir();
     
      int a;
      if (strcmp (buf,"1") == 0)
      {
	cont=0;
	send(newfd, "", 1, 0);
	recibir();
	printf("lineas a procesar: %s\n",buf);
	loop = atoi(buf);
	send(newfd, "", 1, 0);
	while(cont < loop)
	{
	  cont++;
	  recibir();
	  
	  token = strtok(buf, " ");
	  if(((int)token[0]) < 123 && ((int)token[0]) > 96)
	  token[0]=((char)((int)token[0]-32));
	  strcat(vector,token);
	  strcat(vector," ");
	  while(token != NULL)
	  {
	    token = strtok(NULL, " ");
	    if(token!=NULL)
	    {
	      if(((int)token[0]) < 123 && ((int)token[0]) > 96)
	      token[0]=((char)((int)token[0]-32));
	      strcat(vector,token);
	      strcat(vector," ");
	    }
	  }

	  send(newfd, vector, MAX, 0);
	  memset(vector,0,MAX);
	  memset(buf,0,MAXBUFLEN);
	}
	printf("Listo!!! se han cambiado todas las primer letras a Mayuscula\n\n");
      }
      
      if (strcmp (buf,"2") == 0)
      {
	cont=0;
	send(newfd, "", 1, 0);
	recibir();
	printf("lineas a procesar: %s\n",buf);
	loop = atoi(buf);
	send(newfd, "", 1, 0);
	while(cont < loop)
	{
	  cont++;
	  recibir();
	  
	  token = strtok(buf, " ");
	  for(a=0;a<strlen(token);a++)
	  {
	    if(((int)token[a]) < 123 && ((int)token[a]) > 96)
	      token[a]=((char)((int)token[a]-32));
	  }
	  strcat(vector,token);
	  strcat(vector," ");
	  while(token != NULL)
	  {
	    token = strtok(NULL, " ");
	    if(token!=NULL)
	    {
	      for(a=0;a<strlen(token);a++)
	      {
		if(((int)token[a]) < 123 && ((int)token[a]) > 96)
		  token[a]=((char)((int)token[a]-32));
	      }
	      strcat(vector,token);
	      strcat(vector," ");
	    }
	  }

	  send(newfd, vector, MAX, 0);
	  memset(vector,0,MAX);
	  memset(buf,0,MAXBUFLEN);
	}
	printf("Listo!!! se han cambiado todas las letras a MAYUSCULA\n\n");	
      }
      
      if (strcmp (buf,"3") == 0)
      {
	cont=0;
	send(newfd, "", 1, 0);
	recibir();
	printf("lineas a procesar: %s\n",buf);
	loop = atoi(buf);
	send(newfd, "", 1, 0);
	while(cont < loop)
	{
	  cont++;
	  recibir();
	  
	  token = strtok(buf, " ");
	  for(a=0;a<strlen(token);a++)
	  {
	    if(((int)token[a]) < 91 && ((int)token[a]) > 64)
	      token[a]=((char)((int)token[a]+32));
	  }
	  strcat(vector,token);
	  strcat(vector," ");
	  while(token != NULL)
	  {
	    token = strtok(NULL, " ");
	    if(token!=NULL)
	    {
	      for(a=0;a<strlen(token);a++)
	      {
		if(((int)token[a]) < 91 && ((int)token[a]) > 64)
		  token[a]=((char)((int)token[a]+32));
	      }
	      strcat(vector,token);
	      strcat(vector," ");
	    }
	  }

	  send(newfd, vector, MAX, 0);
	  memset(vector,0,MAX);
	  memset(buf,0,MAXBUFLEN);
	}
	printf("Listo!!! se han cambiado todas las letras a minuscula\n\n");		
      }      
      
      if (strcmp (buf,"4") == 0)
      {
	if (send(newfd, "", 1, 0) == -1)
		
	perror("send");
	
	recibir();
	
	num_letra(buf);

	enviar(salida);
      }
      close(newfd);
      exit(0);
    }
    close(newfd);
  }
}

void num_letra(char revisada[])
{
	char unidades[MAX][MAX]={"uno ","dos ","tres ","cuatro ","cinco ","seis ","siete ","ocho ","nueve "};
	char decenas[MAX][MAX]={"diez ","veinte ","treinta ","cuarenta ","cincuenta ","sesenta ","setenta ","ochenta ","noventa "};
	char centenas[MAX][MAX]={"ciento ","doscientos ","trescientos ","cuatrocientos ","quinientos ","seiscientos ","setecientos ","ochocientos ","novecientos "};
	char decenasespeciales[MAX][MAX]={"diez ","once ","doce ","trece ","catorce ","quince ","dieciseis ","diecisiete ","dieciocho ","diecinueve "};

	int numerito;
	int numeritoposterior;
	int i;
	int bandera;
	int posicionactual;

	longitud = strlen(revisada);

	for (i=longitud;i >= 1 ;i--){
		bandera = longitud - i;
		posicionactual = longitud - bandera;

		switch(posicionactual){
			case 1:case 4:case 7: 

				actual[0] = revisada[bandera];actual[1] = '\0';
				numerito = atoi(actual);
				if (numerito != 1){
					strcat(salida,unidades[numerito-1]);
				}
				else{
					if (longitud == 4 && posicionactual == 4){
					}
					else if(longitud == 7 && posicionactual == 7){
						strcat(salida,"un ");
					}else{
						strcat(salida,unidades[numerito-1]);
					}
				}
				break;

			case 2:case 5:case 8: 

				actual[0] = revisada[bandera];actual[1] = '\0';
				numerito = atoi(actual);
				actualposterior[0] = revisada[bandera+1];actualposterior[1] = '\0';
				numeritoposterior = atoi(actualposterior);
				if (numerito == 1){
					if (numeritoposterior != 0){
						strcat(salida,decenasespeciales[numeritoposterior]);
						i--;
					}else{
						strcat(salida,decenas[numerito-1]);
					}
				}
				else{
					strcat(salida,decenas[numerito-1]);
					if (numeritoposterior !=0 && numerito != 0) strcat(salida,"y ");
				}

				break;

			case 3:case 6: 

				actual[0] = revisada[bandera];actual[1] = '\0';
				numerito = atoi(actual);
				if (posicionactual == 6 && longitud > 6){
					if (longitud == 7 && revisada[bandera-1] == '1'){
						strcat(salida,"millon ");
					}else{
					strcat(salida,"millones ");
					}
				}
				else if (posicionactual == 3 && longitud > 3){
					if(revisada[bandera-1] =='0' && revisada[bandera-2] == '0' && revisada[bandera-3] == '0'){
					}else{
					strcat(salida,"mil ");}
				}
				if (numerito == 1 && revisada[bandera+1] == '0' && revisada[bandera+2] == '0'){
					strcat(salida,"cien ");
				}else{
					strcat(salida,centenas[numerito - 1]);
				}
				break;

		}

	}
	if (strcmp (salida,"") == 0)
	  strcat(salida,"cero");

} 