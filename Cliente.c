// CLIENTE.XOCHITL LAISHA LOPEZ LOPEZ

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <iostream>

#define PORT 3490 				

#define MAXBUFLEN 100  		
#define MAX 100

using namespace std;
    
char num[10],opcn[1];
int sockfd, numbytes;  
char buf[MAXBUFLEN];
struct hostent *he;
struct sockaddr_in their_addr; 	
size_t len;

FILE *archivo;
FILE *archivo2;
char name[MAX],name2[MAX];;
char linea[MAX];
int cont_line;
char loop[10];

void crear_socket();
void menu();
void conectar();
void atributos();
void host_name(char *argv[]);
void enviar_opcion();
void enviar(char cadena[]);
void recibir();
void enviar_lineas();

void enviar_lineas()		
{
  cont_line=-1;				
  while(!feof(archivo))		
  {
    fgets(linea,100,archivo);
    cont_line++;
  }
  
  sprintf(loop,"%d",cont_line);
  enviar(loop);
  recibir();
  
  archivo=fopen(name,"r");
 
  archivo2=fopen(name2,"w+");
  
  while(!feof(archivo))
  {
    fgets(linea,100,archivo);
    if(!feof(archivo))
    {
      enviar(linea);
      recibir();
      fprintf(archivo2,"%s",buf);
    }
  }
  
  fclose(archivo);
  fclose(archivo2);
}
  
void menu()
{
  int opc,opc2;
  
  printf("***************\n");
  printf("* 1. Archivos *\n");
  printf("* 2. Numeros  *\n");
  printf("* 3. Salir    *\n");
  printf("***************\n\n");
  scanf("%d",&opc);
  switch(opc)
  {
    case 1:
      
    cout << "Escriba el nombre del archivo que desea abrir: ";
    cin >> name;
    strcpy(name2,name);
    strcat(name2,"2");
    strcat(name,".txt");
    strcat(name2,".txt");
    archivo=fopen(name,"r");
    
    cout << "		1. InitCap" << endl;
    cout << "		2. UpCap" << endl;
    cout << "		3. LowCap" << endl;
    cout << "		4. Volver" << endl;
    cin >> opc2;
  
    switch(opc2)
    {
      case 1:
	opcn[0]='1';
	enviar_opcion();
	recibir();
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las primer letras a Mayuscula" << endl;
      break;
      
      case 2:
	opcn[0]='2';
	enviar_opcion();
	recibir();
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las letras a MAYUSCULA" << endl;
      break;
      
      case 3:
	opcn[0]='3';
	enviar_opcion();
	recibir();
	enviar_lineas();
	cout << "Listo!!! se han cambiado todas las letras a minuscula" << endl;
      break;
      
      case 4:

      break;
    }
    break;
    
    case 2:
    opcn[0]='4';
    cout << "Digite un numero: ";
    cin >> num;
    enviar_opcion();
    recibir();
    enviar(num);
    recibir();
    printf("Recivido: %s\n",buf);
    break;
    
    case 3:
      exit(1);
    break;
  }
}

void host_name(char *argv[])
{
  if ((he=gethostbyname(argv[1])) == NULL)
  {  
    perror("gethostbyname");
    exit(1);
  }
}

void crear_socket()
{
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
  {
    perror("socket");
    exit(1);
  }	
}

void conectar()
{
  if (connect(sockfd, (struct sockaddr *)&their_addr,sizeof(struct sockaddr)) == -1) 
  {
    perror("connect");
    exit(1);
  }
}

void atributos()
{
  len = sizeof(struct sockaddr_in);
  their_addr.sin_family = AF_INET;    
  their_addr.sin_port = htons(PORT);  
  their_addr.sin_addr = *((struct in_addr *)he->h_addr);
  memset(&(their_addr.sin_zero), 8, len); 
}

void enviar_opcion()
{
  if ((send(sockfd, opcn, strlen(opcn), 0)) == -1) 
  {
    perror("send_opc");
    exit(1);
  }
}

void enviar(char cadena[])
{
  if ((send(sockfd, cadena, strlen(cadena), 0)) == -1) 
  {
    perror("send");
    exit(1);
  }	  
}

void recibir()
{
  if ((numbytes=recv(sockfd, buf, MAXBUFLEN, 0)) == -1)
  {
    perror("recv");
    exit(1);
  }
}

int main(int argc, char *argv[])
{
  if (argc != 2) 
  {
    fprintf(stderr,"usaste solo: %d argumento, escribe tambiÃ©n el nombre del servidor\n",argc);
    exit(1);
  }
	
  host_name(argv);
	
  crear_socket();
  
  atributos();
  
  conectar();
  
  menu();

  close(sockfd);
  return 0;
} 